package eu.drori.brightinventionstask.models;

import java.util.Objects;

/**
 * A utility object class representing data that can be returned from the ISBN validation process,
 * such as errors, etc.
 * @see eu.drori.brightinventionstask.util.ISBNValidator#validate(String)
 */
public class ISBNValidationResult {
    /**
     * Whether validation was a success
     */
    private boolean isValid;
    /**
     * ISBN string that should contain only digits (no separators)
     */
    private String isbnDigits;
    /**
     * Represents the error that caused validation to fail
     */
    private ISBNError error;

    /**
     * Returns a new instance of ISBNValidationResult
     * @param isValid the value to set in the new instance
     */
    private ISBNValidationResult(boolean isValid) {
        this.setValid(isValid);
    }

    /**
     * Returns a new instance of ISBNValidationResult
     * @param isValid the value to set in the new instance
     * @param isbnDigits A valid ISBN, which is expected to contain no separators, may be null
     */
    private ISBNValidationResult(boolean isValid, String isbnDigits) {
        this.setValid(isValid);
        this.setIsbnDigits(isbnDigits);
    }

    /**
     * Returns a new instance of ISBNValidationResult
     * @param isValid the value to set in the new instance
     * @param error the error to set in the new instance, may be null
     */
    private ISBNValidationResult(boolean isValid, ISBNError error) {
        this.setValid(isValid);
        this.setError(error);
    }

    /**
     * Convenience method to create a new instance of ISBNValidationResult with a given error.
     * The presence of an error implies that validation failed
     * @param error The error that occured during validation
     * @return A new instance of ISBNValidationResult with isValid set to false and the given error stored
     */
    public static ISBNValidationResult error(ISBNError error) {
        return new ISBNValidationResult(false, error);
    }

    /**
     * Convenience method to create a new instance of ISBNValidationResult with isValid set to true.
     * @return A new instance of ISBNValidationResult with isValid set to true.
     */
    public static ISBNValidationResult valid() {
        return new ISBNValidationResult(true);
    }

    /**
     * Convenience method to create a new instance of ISBNValidationResult with a valid ISBN string containing only digits.
     * The presence of a valid ISBN implies that validation succeeded
     * @param isbnDigits A valid ISBN, which is expected to contain no separators, may be null
     * @return A new instance of ISBNValidationResult with isValid set to true and isbnDigits set to the given ISBN
     */
    public static ISBNValidationResult valid(String isbnDigits) {
        return new ISBNValidationResult(true, isbnDigits);
    }

    /**
     * Check whether this instance represents a successful validation
     * @return Whether this validation result represents a successful validation
     */
    public boolean isValid() {
        return isValid;
    }

    /**
     * Set whether this instance represents a successful validation
     * @param valid whether this validation result represents a successful validation
     */
    protected void setValid(boolean valid) {
        isValid = valid;
    }

    /**
     * Get the valid ISBN that was given as a result of a successful validation, expected to be without separators
     * @return The ISBN received during validation, may be null
     * @see eu.drori.brightinventionstask.util.ISBNValidator#validate(String)
     */
    public String getIsbnDigits() {
        return isbnDigits;
    }

    /**
     * Set the valid ISBN that was given as a result of a successful validation, expected to be without separators
     * @param isbnDigits The ISBN received during validation without any separators
     * @see eu.drori.brightinventionstask.util.ISBNValidator#validate(String)
     */
    public void setIsbnDigits(String isbnDigits) {
        this.isbnDigits = isbnDigits;
    }

    /**
     * Get the stored {@link ISBNError}
     * @return The stored {@link ISBNError} in this instance, may be null
     */
    public ISBNError getError() {
        return error;
    }

    /**
     * Set a new error for the {@link ISBNValidationResult} instance
     * @param error The error to set, can be null
     * @see ISBNError
     */
    public void setError(ISBNError error) {
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ISBNValidationResult that = (ISBNValidationResult) o;
        return isValid == that.isValid &&
                error == that.error;
    }

    @Override
    public int hashCode() {
        return Objects.hash(isValid, error);
    }

    /**
     * Returns a String representation of the instance in the following format
     * <pre>
     *     ISBNValidationResult{isValid=true/false,error=null/error message}
     * </pre>
     * @return String representation of the instance
     * @see ISBNError#getMessage()
     */
    @Override
    public String toString() {
        return "ISBNValidationResult{" +
                "isValid=" + isValid +
                ", error=" + (error != null ? error.getMessage() : null) +
                '}';
    }
}
