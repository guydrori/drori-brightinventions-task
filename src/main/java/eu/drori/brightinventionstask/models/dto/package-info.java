/**
 * Package containing DTO models that are to be serialized and deserialized to and from JSON.
 */
package eu.drori.brightinventionstask.models.dto;