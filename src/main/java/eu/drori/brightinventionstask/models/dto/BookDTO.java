package eu.drori.brightinventionstask.models.dto;

import eu.drori.brightinventionstask.models.Book;

import javax.validation.constraints.NotNull;

/**
 * A DTO class to be used when serializing and deserializing to and from JSON
 */
public class BookDTO {
    /**
     * The book's ISBN, should not be null when possible
     */
    @NotNull
    public String ISBN;
    /**
     * The book's ISBN, should not be null when possible
     */
    @NotNull
    public String title;
    /**
     * The author's name, should not be null when possible
     */
    @NotNull
    public String author;
    /**
     * The book's rating
     */
    public Integer rating;
    /**
     * The number of pages in the book
     */
    @NotNull
    public Integer numberOfPages;

    public BookDTO() {
    }

    /**
     * Creates a new instance of BookDTO and populates it with data from the given {@link Book} instance
     * @param book The {@link Book} instance from which to populate data
     */
    public BookDTO(Book book) {
        if (book == null) {
            throw new IllegalArgumentException("Book cannot be null");
        }
        this.ISBN = book.getISBN();
        this.title = book.getTitle();
        this.author = book.getAuthor().getName();
        this.rating = book.getRating();
        this.numberOfPages = book.getNumberOfPages();
    }
}
