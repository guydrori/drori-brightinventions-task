/**
 * Package containing model classes that are used in the application, both database entities and plain Java data classes.
 */
package eu.drori.brightinventionstask.models;