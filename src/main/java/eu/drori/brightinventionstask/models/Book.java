package eu.drori.brightinventionstask.models;

import eu.drori.brightinventionstask.models.dto.BookDTO;
import eu.drori.brightinventionstask.util.ISBNValidator;
import org.apache.logging.log4j.util.Strings;

import javax.persistence.*;
import java.util.Locale;
import java.util.Objects;

/**
 * Entity class representing how books should be stored in the database
 */
@Entity
public class Book {
    /**
     * The book's ISBN
     */
    @Id
    public String ISBN;

    /**
     * The book's title
     */
    @Column(nullable = false)
    public String title;

    /**
     * Foreign key to the book's author
     * @see Author
     */
    @ManyToOne(fetch = FetchType.EAGER)
    public Author author;

    /**
     * The book's rating which should be either null or between 1 and 5.
     */
    public Integer rating;

    /**
     * The number of pages in the book, which should be greater than 0.
     */
    @Column(nullable = false)
    public int numberOfPages;

    public Book() {

    }

    /**
     * A constructor that copies data from a {@link BookDTO} instance
     * @param bookDTO A {@link BookDTO} instance containing data that should be copied to the Book instance
     */
    public Book(BookDTO bookDTO) {
        setISBN(bookDTO.ISBN);
        setTitle(bookDTO.title);
        setRating(bookDTO.rating);
        setNumberOfPages(bookDTO.numberOfPages);
    }

    /**
     * Get the book's ISBN
     * @return The book's ISBN
     */
    public String getISBN() {
        return ISBN;
    }

    /**
     * Set a new ISBN for the Book instance as long as it is valid.
     * @param ISBN The ISBN to set
     * @see ISBNValidator#validate(String)
     */
    public void setISBN(String ISBN) {
        if (Strings.isBlank(ISBN)) {
            throw new IllegalArgumentException("ISBN must be provided!");
        }
        ISBNValidationResult isbnValidationResult = ISBNValidator.validate(ISBN);
        if (!isbnValidationResult.isValid()) {
            throw new IllegalArgumentException(isbnValidationResult.getError().getMessage());
        } else {
            this.ISBN = isbnValidationResult.getIsbnDigits();
        }
    }

    /**
     * Get the book's title
     * @return The book's title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the book's title
     * @param title The book's title, must not be empty or null
     */
    public void setTitle(String title) {
        if (Strings.isBlank(title)) {
            throw new IllegalArgumentException("Title must be provided");
        }
        this.title = title;
    }

    /**
     * Get the book's author
     * @return The book's author
     */
    public Author getAuthor() {
        return author;
    }

    /**
     * Set the book's author
     * @param author The book's author as a database entity.
     * @see Author
     */
    public void setAuthor(Author author) {
        if (author == null) {
            throw new IllegalArgumentException("Author must be provided!");
        }
        this.author = author;
    }

    /**
     * Get the book's rating
     * @return The book's rating
     */
    public Integer getRating() {
        return rating;
    }

    /**
     * Set the book's rating
     * @param rating The book's rating, can be either null or a number between 1 and 5
     */
    public void setRating(Integer rating) {
        if (rating != null && (rating < 1 || rating > 5)) {
            throw new IllegalArgumentException("Invalid rating, must be between 1 and 5");
        }
        this.rating = rating;
    }

    /**
     * Get the number of pages of the Book from this instance
     * @return The number of pages in the book
     */
    public int getNumberOfPages() {
        return numberOfPages;
    }

    /**
     * Set the number of pages for this book
     * @param numberOfPages The number of pages in the book, cannot be lower than 1
     */
    public void setNumberOfPages(int numberOfPages) {
        if (numberOfPages < 1) {
            throw new IllegalArgumentException("Number of pages cannot be less than 1!");
        }
        this.numberOfPages = numberOfPages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return numberOfPages == book.numberOfPages &&
                ISBN.equals(book.ISBN) &&
                title.equals(book.title) &&
                author.equals(book.author) &&
                Objects.equals(rating, book.rating);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ISBN, title, author, rating, numberOfPages);
    }
}
