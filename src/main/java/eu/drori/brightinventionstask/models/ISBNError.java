package eu.drori.brightinventionstask.models;

/**
 * An enum representing possible errors that can occur during ISBN validation.
 * Each enum value contains an error message
 * @see eu.drori.brightinventionstask.util.ISBNValidator#validate(String)
 */
public enum ISBNError {
    BLANK("ISBN is blank"),
    INVALID_CHARACTERS("ISBN has invalid characters, must contain only digits and dashes, or X for last digit"),
    INVALID_LENGTH("Invalid number of digits"),
    INVALID_CHECK_DIGIT("Invalid check digit");

    /**
     * The error message
     */
    private final String message;

    /**
     * Creates a new enum value with the given String set as its error message
     * @param message The error message for this enum value
     */
    ISBNError(String message) {
        this.message = message;
    }

    /**
     * Get the error message for this enum value
     * @return The error message
     */
    public String getMessage() {
        return this.message;
    }
}
