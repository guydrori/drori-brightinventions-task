/**
 * Package containing Spring Data repository definitions.
 */
package eu.drori.brightinventionstask.repositories;