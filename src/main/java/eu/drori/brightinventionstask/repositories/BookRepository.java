package eu.drori.brightinventionstask.repositories;

import eu.drori.brightinventionstask.models.Author;
import eu.drori.brightinventionstask.models.Book;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.lang.NonNull;

/**
 * JPA repository for the Book entity
 * @see Book
 */
public interface BookRepository extends PagingAndSortingRepository<Book, String> {
    /**
     * Queries the database for the number of books associated with a given {@link Author} entity
     * @param author The {@link Author} entity to be used in the count query
     * @return the number of Book records in the database who are associated with the given author
     */
    Long countByAuthor(@NonNull Author author);
}
