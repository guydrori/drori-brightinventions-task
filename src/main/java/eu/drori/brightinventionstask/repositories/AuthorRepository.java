package eu.drori.brightinventionstask.repositories;

import eu.drori.brightinventionstask.models.Author;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;

/**
 * JPA repository for the Author entity
 * @see Author
 */
public interface AuthorRepository extends CrudRepository<Author, Long> {
    /**
     * Finds an {@link Author} entity in the database with its name equals to the given name
     * @param name the name to search in the database, shouldn't be null
     * @return An {@link Optional} instance informing whether an Author entity with the given name exists
     */
    Optional<Author> findByName(@NonNull String name);
}
