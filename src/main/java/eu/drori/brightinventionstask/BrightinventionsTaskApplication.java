package eu.drori.brightinventionstask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The main class for the application
 */
@SpringBootApplication
public class BrightinventionsTaskApplication {

    /**
     * Starts the Spring Boot application
     * @param args Command line arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(BrightinventionsTaskApplication.class, args);
    }

}
