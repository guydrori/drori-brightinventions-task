package eu.drori.brightinventionstask.util;

import eu.drori.brightinventionstask.models.ISBNError;
import eu.drori.brightinventionstask.models.ISBNValidationResult;
import org.apache.logging.log4j.util.Strings;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * A utility class for validating ISBNs. Unit tests available.
 */
public class ISBNValidator {
    /**
     * A regular expression used to verify whether an ISBN string does not contain any invalid characters
     */
    public static final Pattern ISBN_CHARACTERS_PATTERN = Pattern.compile("^(\\d+-?)+[Xx]?$");

    /**
     * Validates a given ISBN
     * @param ISBN The ISBN to validate
     * @return A {@link ISBNValidationResult} object containing information about whether the information was a success
     * or whether any errors occured.
     */
    public static ISBNValidationResult validate(String ISBN) {
        if (Strings.isBlank(ISBN)) return ISBNValidationResult.error(ISBNError.BLANK);
        if (!ISBN_CHARACTERS_PATTERN.matcher(ISBN).matches()) {
            return ISBNValidationResult.error(ISBNError.INVALID_CHARACTERS);
        }
        return validateCheckDigit(ISBN.replace("-", ""));
    }

    /**
     * Validates the check digit of the given ISBN
     * @param isbnDigits The ISBN to validate, given as a String without any separators (dashes)
     * @return An {@link ISBNValidationResult} instance representing the result of the check digit validation.
     * its isbnDigits value should be set to the isbnDigits parameter.
     * A failed validation should return an instance with its {@link ISBNError} set to {@link ISBNError#INVALID_CHECK_DIGIT}
     */
    private static ISBNValidationResult validateCheckDigit(String isbnDigits) {
        if (isbnDigits.length() == 10) {
            return isISBN10CheckDigitValid(isbnDigits);
        } else if (isbnDigits.length() == 13) {
            return isISBN13CheckDigitValid(isbnDigits);
        } else {
            return ISBNValidationResult.error(ISBNError.INVALID_LENGTH);
        }
    }

    /**
     * Validates a 13 digit ISBN based on the method defined in the latest ISBN Users' Manual
     * @param isbnDigits The ISBN to validate, given as a String without any separators (dashes)
     * @return An {@link ISBNValidationResult} instance representing the result of the check digit validation.
     *  * its isbnDigits value should be set to the isbnDigits parameter.
     *      * A failed validation should return an instance with its {@link ISBNError} set to {@link ISBNError#INVALID_CHECK_DIGIT}
     * @see <a href="https://www.isbn-international.org/content/isbn-users-manual">ISBN User's Manual</a>
     */
    private static ISBNValidationResult isISBN13CheckDigitValid(String isbnDigits) {
        List<Integer> digits = isbnDigits.chars().map(Character::getNumericValue).boxed().collect(Collectors.toList());
        int sumOfDigitsWithWeights = 0;
        for (int i = 0; i < (digits.size() - 1); i++) {
            if (i % 2 == 0) {
                sumOfDigitsWithWeights += digits.get(i);
            } else {
                sumOfDigitsWithWeights += (digits.get(i) * 3);
            }
        }
        int mod10 = sumOfDigitsWithWeights % 10;
        if (mod10 == 0) {
            return digits.get(12) == 0 ? ISBNValidationResult.valid(isbnDigits) : ISBNValidationResult.error(ISBNError.INVALID_CHECK_DIGIT);
        } else {
            return digits.get(12) == (10 - mod10) ? ISBNValidationResult.valid(isbnDigits) : ISBNValidationResult.error(ISBNError.INVALID_CHECK_DIGIT);
        }
    }

    /**
     * Validates a 10 digit ISBN based on the method defined in the fourth edition of the ISBN Users' Manual
     * @param isbnDigits The ISBN to validate, given as a String without any separators (dashes)
     * @return An {@link ISBNValidationResult} instance representing the result of the check digit validation.
     *  * its isbnDigits value should be set to the isbnDigits parameter.
     *      * A failed validation should return an instance with its {@link ISBNError} set to {@link ISBNError#INVALID_CHECK_DIGIT}
     * @see <a href="https://web.archive.org/web/20130522043458/http://www.isbn.org/standards/home/isbn/international/html/usm4.htm">ISBN User's Manual - Fourth Edition</a>
     */
    private static ISBNValidationResult isISBN10CheckDigitValid(String isbnDigits) {
        List<Integer> digits = isbnDigits.substring(0, 9).chars().map(Character::getNumericValue).boxed().collect(Collectors.toList());
        int lastDigit = isbnDigits.charAt(9) == 'X' || isbnDigits.charAt(9) == 'x' ? 10 : Character.getNumericValue(isbnDigits.charAt(9));
        int sumOfDigitsWithWeights = 0;
        for (int i = 0, weight = 10; i < digits.size(); i++, weight--) {
            sumOfDigitsWithWeights += (digits.get(i) * weight);
        }
        sumOfDigitsWithWeights += lastDigit;
        return sumOfDigitsWithWeights % 11 == 0 ? ISBNValidationResult.valid(isbnDigits) : ISBNValidationResult.error(ISBNError.INVALID_CHECK_DIGIT);
    }
}
