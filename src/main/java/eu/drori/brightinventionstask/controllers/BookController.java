package eu.drori.brightinventionstask.controllers;

import eu.drori.brightinventionstask.models.Author;
import eu.drori.brightinventionstask.models.Book;
import eu.drori.brightinventionstask.models.ISBNValidationResult;
import eu.drori.brightinventionstask.models.dto.BookDTO;
import eu.drori.brightinventionstask.repositories.AuthorRepository;
import eu.drori.brightinventionstask.repositories.BookRepository;
import eu.drori.brightinventionstask.util.ISBNValidator;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * A rest controller class for REST endpoints regarding CRUD operations on books in the database
 * @see Book
 */
@RestController
@RequestMapping("/books")
public class BookController {
    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;

    @Autowired
    public BookController(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    /**
     * Gets a page from the list of books from the database
     * @param pageable the configuration of the desired page
     * @return A page containing {@link BookDTO} objects to serialize into JSON
     */
    @GetMapping("/")
    @Transactional(rollbackFor = Exception.class)
    public Page<BookDTO> list(Pageable pageable) {
        return bookRepository.findAll(pageable).map(BookDTO::new);
    }

    /**
     * Inserts a book into the database
     * @param bookDTO A validated {@link BookDTO} instance containing data to be stored in the database
     * @return A ResponseEntity containing the {@link BookDTO} entity given.
     * The HTTP Status Code informs whether the operation was successful or not
     * <ul>
     *     <li><b>302 Found</b> - returned when the book already exists in the database</li>
     *     <li><b>201 Created</b> - returned when the book has been successfully added to the database</li>
     * </ul>
     */
    @PostMapping("/")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<BookDTO> insert(@RequestBody @Validated BookDTO bookDTO) {
        ISBNValidationResult isbnValidationResult = ISBNValidator.validate(bookDTO.ISBN);
        if (!isbnValidationResult.isValid()) {
            throw new IllegalArgumentException(isbnValidationResult.getError().getMessage());
        }
        Optional<Book> bookQuery = bookRepository.findById(isbnValidationResult.getIsbnDigits());
        if (bookQuery.isPresent()) {
            return ResponseEntity.status(HttpStatus.FOUND).body(new BookDTO(bookQuery.get()));
        } else {
            Book book = new Book(bookDTO);
            book.setAuthor(getAuthorEntityFromBookDTO(bookDTO));
            bookRepository.save(book);
            return ResponseEntity.status(HttpStatus.CREATED).body(bookDTO);
        }
    }

    /**
     * Checks whether an Author entity exists for the author name inside the {@link BookDTO} instance given.
     * If it exists, then it is returned. If not, a new {@link Author} record is added to the database with the given author name
     * @param bookDTO A BookDTO instance containing the author name to verify
     * @return An {@link Author} instance corresponding to the given author name from the {@link BookDTO} instance given
     */
    private Author getAuthorEntityFromBookDTO(BookDTO bookDTO) {
        if (bookDTO == null) {
            throw new IllegalArgumentException("bookDTO cannot be null");
        }
        if (Strings.isBlank(bookDTO.author)) {
            throw new IllegalArgumentException("Author must be provided");
        }
        Optional<Author> authorQueryResult = authorRepository.findByName(bookDTO.author);
        return authorQueryResult.orElseGet(() -> {
            Author author = new Author(bookDTO.author);
            authorRepository.save(author);
            return author;
        });
    }

    /**
     * Edits a book inserted into the database
     * @param ISBN The ISBN of the book to edit
     * @param bookDTO A BookDTO instance containing data values that should be changed in the database.
     *                Any values that shouldn't be altered should be omitted, therefore there is no validation here.
     * @return A ResponseEntity containing the {@link BookDTO} instance corresponding to the data currently stored in the database
     * The HTTP Status Code informs whether the operation was successful or not
     * <ul>
     *     <li><b>404 Not Found</b> - returned when the book does not exist in the database (with no request body)</li>
     *     <li><b>200 OK</b> - returned when the book has been successfully updated in the database</li>
     * </ul>
     */
    @PutMapping("/{isbn}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<BookDTO> update(@PathVariable("isbn") String ISBN, @RequestBody BookDTO bookDTO) {
        ISBNValidationResult isbnValidationResult = ISBNValidator.validate(ISBN);
        if (!isbnValidationResult.isValid()) {
            throw new IllegalArgumentException(isbnValidationResult.getError().getMessage());
        }
        Optional<Book> bookQueryResult = bookRepository.findById(isbnValidationResult.getIsbnDigits());
        if (bookQueryResult.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            Book book = bookQueryResult.get();
            if (bookDTO.author != null && !bookDTO.author.equals(book.getAuthor().getName())) {
                book.setAuthor(getAuthorEntityFromBookDTO(bookDTO));
            }
            if (bookDTO.rating != null && !bookDTO.rating.equals(book.getRating())) {
                book.setRating(bookDTO.rating);
            }
            if (bookDTO.numberOfPages != null && bookDTO.numberOfPages != book.getNumberOfPages()) {
                book.setNumberOfPages(bookDTO.numberOfPages);
            }
            if (bookDTO.title != null && !bookDTO.title.equals(book.getTitle())) {
                book.setTitle(bookDTO.title);
            }
            bookRepository.save(book);
            return ResponseEntity.ok(new BookDTO(book));
        }
    }

    /**
     * Deletes a book from the database
     * @param ISBN The ISBN of the book to delete
     * @return A ResponseEntity informing which status code to return.
     * The status code returned depends on the status of the operation.
     * <ul>
     *     <li><b>404 Not Found</b> - returned when the book does not exist in the database (with no request body)</li>
     *     <li><b>202 NOo Content</b> - returned when the book has been successfully deleted from the database</li>
     * </ul>
     */
    @DeleteMapping("/{isbn}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Void> delete(@PathVariable("isbn") String ISBN) {
        ISBNValidationResult isbnValidationResult = ISBNValidator.validate(ISBN);
        if (!isbnValidationResult.isValid()) {
            throw new IllegalArgumentException(isbnValidationResult.getError().getMessage());
        }
        Optional<Book> bookQueryResult = bookRepository.findById(isbnValidationResult.getIsbnDigits());
        if (bookQueryResult.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            Book book = bookQueryResult.get();
            Author author = book.getAuthor();
            bookRepository.delete(book);
            if (bookRepository.countByAuthor(author) == 0) {
                authorRepository.delete(author);
            }
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }
}
