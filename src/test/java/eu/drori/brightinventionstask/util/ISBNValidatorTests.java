package eu.drori.brightinventionstask.util;


import eu.drori.brightinventionstask.models.ISBNError;
import eu.drori.brightinventionstask.models.ISBNValidationResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Test class for testing the functionality of the ISBNValidator utility class
 * @see ISBNValidator
 */
public class ISBNValidatorTests {

    @Test
    void validateCorrectISBN10() {
        Assertions.assertEquals(ISBNValidationResult.valid(), ISBNValidator.validate("0-306-40615-2"));
    }

    @Test
    void validateCorrectISBN10WithX() {
        Assertions.assertEquals(ISBNValidationResult.valid(), ISBNValidator.validate("043942089X"));
    }

    @Test
    void validateIncorrectISBN10WithWrongLastLetter() {
        Assertions.assertEquals(ISBNValidationResult.error(ISBNError.INVALID_CHARACTERS), ISBNValidator.validate("043942089A"));
    }

    @Test
    void validateIncorrectISBN10() {
        Assertions.assertEquals(ISBNValidationResult.error(ISBNError.INVALID_CHECK_DIGIT), ISBNValidator.validate("0-306-40615-3"));
    }

    @Test
    void validateIncorrectISBNFormatLetters() {
        Assertions.assertEquals(ISBNValidationResult.error(ISBNError.INVALID_CHARACTERS), ISBNValidator.validate("Hello, world!"));
    }

    @Test
    void validateCorrectISBN13() {
        Assertions.assertEquals(ISBNValidationResult.valid(), ISBNValidator.validate("978-0-306-40615-7"));
    }

    @Test
    void validateIncorrectISBN13() {
        Assertions.assertEquals(ISBNValidationResult.error(ISBNError.INVALID_CHECK_DIGIT), ISBNValidator.validate("978-0-306-40615-4"));
    }

    @Test
    void validateIncorrectISBNLength() {
        Assertions.assertEquals(ISBNValidationResult.error(ISBNError.INVALID_LENGTH), ISBNValidator.validate("978-0-306-40615-"));
        Assertions.assertEquals(ISBNValidationResult.error(ISBNError.INVALID_LENGTH), ISBNValidator.validate("0-306-40615-"));
    }

    @Test
    void validateBlankString() {
        Assertions.assertEquals(ISBNValidator.validate(""), ISBNValidationResult.error(ISBNError.BLANK));
    }

    @Test
    void validationReturnsCorrectISBNDigitsString() {
        ISBNValidationResult isbnValidationResult = ISBNValidator.validate("0-306-40615-2");
        ISBNValidationResult expected = ISBNValidationResult.valid("0306406152");
        Assertions.assertEquals(expected,isbnValidationResult);
        Assertions.assertEquals(expected.getIsbnDigits(),isbnValidationResult.getIsbnDigits());
    }

}
