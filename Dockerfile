FROM maven:3.6.3-openjdk-14
WORKDIR /app
COPY pom.xml .
ADD src ./src
RUN mvn clean package

FROM openjdk:14
WORKDIR /app
COPY --from=0 /app/target/brightinventions-task-0.0.1-SNAPSHOT.jar .
CMD ["java","-jar","brightinventions-task-0.0.1-SNAPSHOT.jar"]
