# README #

Guy Drori's task submission for Bright Inventions.

* Javadocs are available in the javadocs/ directory
* Postman request collection available in the Bright Inventions Task - Guy Drori.postman_collection.json file

## Running the application ##
The application can be run either using Docker-Compose or directly using an IDE or the command line.

It requires a MySQL database and requires the following environment variables to be set. When using Docker-Compose, these variables are set for you :)
```
JDBC_URL=The JDBC URL to use to connect to the database
DB_USER=The username to be used when connecting to the database
DB_PASS=The password to be used when connecting to the database
```

## Docker-Compose ##
The whole application can be started using the provided Docker-Compose file, by running the following command
```bash
docker-compose up
```

The application will then be available on the host machine at port 8080, and its associated MySQL server at port 3306.

Another docker-compose file is available for convenience, it can be used to start MySQL only
```bash
docker-compose -f mysql-docker-compose.yml up
```

This will make MySQL available on the host machine at port 3306


## IDE ##
The project can be run in an IDE by running the following class, after importing the project as a Maven project using the pom.xml file.
Java 14 is required.
```
eu.drori.brightinventionstask.BrightinventionsTaskApplication 
```

The HTTP server will then be available at port 8080 on your local machine.

## JAR ##
Provided that Java 14 is installed on your machine, the application can be run using the provided jar file as follows.
```bash
java -jar drori-brightinventions-task.jar
```
or
```bash
<path to java 14 java binary> -jar drori-brightinventions-task.jar
```
The HTTP server will then be available at port 8080 on your local machine.

# Book schema #
The schema of the Book data transfer object that is sent and received by the application as JSON is as follows
```json
{
    "ISBN": "9780446310789" //string,
    "title": "To Kill a Mockingbird" //string,
    "author": "Harper Lee" //string - author name,
    "rating": 4 //int - can be null,
    "numberOfPages": 384 //number of pages
}
```